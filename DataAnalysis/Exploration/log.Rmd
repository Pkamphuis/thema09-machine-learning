---
title: "Logboek Themaopdracht 9"
author: "Priscilla Kamphuis"
date: "November 14, 2018"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

/*
* Copyright (c) 2018 <Priscilla Kamphuis>.
* Licensed under GPLv3. See gpl.md
*/

```{r warning = FALSE}
getwd()
setwd("~/thema09_pkamphuis")
```

```{r, include = FALSE}
library(ggplot2)
library(reshape2)
library(pheatmap)
library(pander)
```

#Week 1
In week 1 heb ik een onderwerp gekozen voor dit onderzoek, namelijk borstkanker. De dataset die hierbij zit bestaat uit 11 attributen en 699 rijen. Het eerste attribuut geeft een ID nummer aan die verwijst naar een patiënt. De 9 attributen hierna bevatten bepaalde celkenmerken met daarbij een ID nummer van 1 tot 10. De laatste kolom bestaat uit de klasse waarin de tumor valt, te verdelen in goedaardige en kwaadaardige tumoren. Dit is aangegeven met een 2 voor een goedaardige tumor en een 4 voor een kwaadaardige tumor. De verdeling tussen deze twee groepen is niet helemaal gelijk, want er zijn 458 patiënten in opgenomen die een goedaardige tumor hebben en 241 patiënten zijn hierin opgenomen met een kwaadaardige tumor. De percentages zijn respectievelijk 65.5% en 34.5%.
De attributen bevatten alle elf het datatype integer, dit betekent dat het allemaal getallen zijn zonder decimalen na een komma.
Deze dataset bevatte in eerste instantie geen kolomnamen, maar die heb ik in het begin van het script toegevoegd, zodat het beter leesbaar is.
De dataset is verkregen uit klinisch onderzoek, verzameld door Dr. Wolberg. De data is verkregen tussen januari 1989 en november 1991. De dataset die in dit onderzoek gebruikt wordt, is gepubliceerd in 1992.
De onderzoeksvraag die beantwoord gaat worden in dit onderzoek is: 
- Is het mogelijk om een model te maken, in de komende zeven weken, die met 95% betrouwbaarheid kan zeggen of de patiënt een goedaardige of kwaadaardige tumor heeft aan de hand van negen kenmerken?

Het bestand is ingeladen als dataset in Rmarkdown. Hieronder is overzichtelijk weergegeven wat er in ieder attribuut staat:


   #  Attribute                     Domain
   -- -----------------------------------------
   1. Sample code number            id number
   2. Clump Thickness               1 - 10
   3. Uniformity of Cell Size       1 - 10
   4. Uniformity of Cell Shape      1 - 10
   5. Marginal Adhesion             1 - 10
   6. Single Epithelial Cell Size   1 - 10
   7. Bare Nuclei                   1 - 10
   8. Bland Chromatin               1 - 10
   9. Normal Nucleoli               1 - 10
  10. Mitoses                       1 - 10
  11. Class:                        (2 for benign, 4 for malignant)
  
De missende waarden in de dataset worden aangegeven met "?" en dit is in het begin van het script aangepast naar NA.


Hieronder wordt het bestand ingelezen. Het bestand krijgt de naam dataset1.

```{r}
dataset1 <- read.csv("../data/breast-cancer-wisconsin.csv", 
                     header = FALSE, 
                     sep = ",", 
                     na.strings = "?")
```

Hieronder zijn attribuutnamen toegevoegd om het beter leesbaar te maken.
```{r}
colnames(dataset1) <- c("SampleNumber",
                        "ClumpThickness",
                        "UniformityOfCellSize",
                        "UniformityOfCellShape",
                        "MarginalAdhesion",
                        "SingleEpithelialCellSize",
                        "BareNuclei",
                        "BlandChromatin",
                        "NormalNucleoli",
                        "Mitoses",
                        "Class")
```


Hieronder zijn de exacte getallen weergegeven van het aantal patiënten met een goedaardige en een kwaadaardige tumor. Hieruit is te halen dat er 458 patiënten zijn met een goedaardige tumor en 241 patiënten hebben een kwaadaardige tumor. Er zijn dus in deze dataset meer patiënten met een goedaardige tumor dan met een kwaadaardige tumor.

```{r}
length(subset(dataset1, Class == 2)[,1])
length(subset(dataset1, Class == 4)[,1])
```



#Week 2
De eerste figuren gemaakt om de dataset duidelijk weer te geven. Hieruit blijkt ook dat de twee klassen ongelijk vertegenwoordigd zijn. Er zijn 458 patiënten uit de dataset met een goedaardige tumor en 241 patiënten met een kwaadaardige tumor. Uit de analyse blijkt ook dat er in het attribuut "Bare Nuclei" 16 missende waarden zijn. Deze worden aangegeven met "?". De rijen met missende waarden zijn bij een aantal plots verwijderd. 
Er zijn een aantal outliers in de attributen. Dit is duidelijk te zien in de plot met de negen boxplots (van ieder attribuut een boxplot). Het attribuut "Clump Thickness" is de enige zonder outliers en het attribuut "Bland chromatin" bevat maar een outlier. De rest van de attributen bevatten meer outliers. De outliers bevinden zich voornamelijk bij de goedaardige tumoren. Hierbij is op te merken dat de boxplot van de kwaadaardige tumor in bijna alle gevallen de volledige y-as omvat. Het eerste kwartiel zit vaak op ID nummer 1 en het vierde kwartiel zit vaak op ID nummer 10. Hierdoor zijn er natuurlijk geen outliers in de boxplots te zien. Om bij de kwaadaardige tumoren goed te zien waar de punten liggen, is er nog een andere plot te vinden in het script, namelijk een jitter plot. Hieruit is duidelijker af te lezen waar de punten allemaal precies liggen. Hierin zijn ook duidelijk weer een aantal outliers te zien.
Er zijn twee attributen die korrelatie laten zien, namelijk uniformity of cell size en uniformity of cell shape. De boxplots van deze twee attributen komen aardig overeen en in de heatmap is te zien dat de vakjes hiervan richting de rode kleur zijn. Dit betekent korrelatie. Om dit nog beter te bekijken is nog een scatter plot gemaakt. Hierin is te zien dat het globaal gezien inderdaad als een diagonale lijn naar boven loopt en dat de lage ID nummers bij beide attributen vaak betekenen dat het om een goedaardige tumor gaat, terwijl de hogere ID nummers vaak laten zien dat het om een kwaadaardige tumor gaat.
Over het algemeen gezien is het bij de meeste attributen wel zo dat de hoge ID nummers vaak een kwaadaardige tumor aangeven, terwijl de lage ID nummers een goedaardige tumor weergeven.
Een transformatie is niet nodig bij deze dataset, omdat het om ID nummers gaat in plaats van bijvoorbeeld lengtes of gewichten.


In deze histogram zijn het aantal patiënten te zien met goedaardige en kwaardaardige tumoren in een grafische weergave. Hieruit is ook af te lezen dat 458 patiënten een goedaardige en 241 patiënten een kwaadaardige tumor hebben.


```{r}
dataset1$ClassName <- ordered(dataset1$Class,
levels = c(2,4),
labels = c("Benign", "Malignant"))

cbbPalette <- c("#66bb6a", "#ef5350")

histogram1 <- ggplot(dataset1) + 
  geom_bar(aes(x = ClassName, colour= ClassName, fill=ClassName)) +
  ggtitle("Amount of patients with benign or malignant tumors in the dataset") +
  xlab("Benign or malignant tumor") +
  ylab("Amount of patients") +
  scale_colour_manual(values=cbbPalette) +
  scale_fill_manual(values=cbbPalette) 

histogram1
```



Hieronder is een samenvatting te zien van de data. Vooraan staan de namen van de kolommen, daarna staat het datatype die in de kolom zit en daarachter staan de waarden per kolom van de eerste rijen weergegeven. Er zijn in totaal 11 attributen en 699 rijen, waar verder in dit script nog een extra rij wordt toegevoegd. Deze rij zal ClassNames heten en bevat nominale waarden, benign en malignant, die aangeven of de tumor goedaardig of kwaadaardig is bij een ID nummer. Deze kolom is gebasseerd op de 11e kolom in de dataset, waar met 2 wordt aangegeven dat een tumor goedaardig is en met 4 wordt aangegeven dat een tumor kwaadaardig is.  

Onderaan worden nog de dimensies weergegeven van de dataset, waarin ook te zien is dat er 699 rijen zijn en 11 kolommen.

```{r}
pander(str(dataset1))
```

```{r}
pander(head(dataset1))
```



Hieronder zijn boxplots te zien van elk attribuut op de y-as en op de x-as weergegeven of het om een kwaadaardige of goedaardige tumor gaat. Hierbij is te zien dat er relatief weinig overlap is bij de meeste attributen, waardoor het voor een machine learning programma makkelijker wordt om het te onderscheiden. Bijvoorbeeld de boxplots uniformity of cell shape en uniformity of cell size zijn goede attributen om te gebruiken. Hier zit duidelijk onderscheid tussen de ID nummers van de goedaardige en kwaadaardige tumor. Mitoses als attribuut is minder goed te gebruiken voor machine learning, omdat daar veel overlap in voorkomt. Het is fijn als er duidelijk verschil zit tussen de ID nummers van kwaadaardige en goedaardige tumoren voor de volgende stappen.

Om deze boxplots zo te krijgen is een nieuwe dataset gemaakt die data.melt heet. Deze bevat vier kolommen: SampleNumber, ClassName, variable en value. In SampleNumber staan de ID nummers van de patiënten, in ClassName is goedaardig of kwaadaardig terug te vinden, in variable staan de attributen en in value de ID waarde van dat attribuut.


```{r warning = FALSE}
data.melt <- melt(dataset1,id.vars=c('SampleNumber','ClassName'),
                  measure.vars=c("ClumpThickness","UniformityOfCellSize",
                        "UniformityOfCellShape","MarginalAdhesion",
                        "SingleEpithelialCellSize","BareNuclei",
                        "BlandChromatin","NormalNucleoli","Mitoses"))

cbbPalette <- c("#66bb6a", "#ef5350")

boxplot1 <- ggplot(data.melt) + 
  geom_boxplot(aes(x = ClassName ,y = value, colour= ClassName))+
  facet_wrap(~variable, ncol=3, scales='free') +
  ggtitle("Correlation between benign/malignant tumor and ID-numbers attributes") +
  xlab("Benign or malignant tumor") +
  ylab("ID numbers") +
  scale_colour_manual(values=cbbPalette)

boxplot1

```

In de plot hieronder zijn plots te zien met puntjes om meer duidelijkheid te geven over de overlap van de punten. Er is hier te zien dat het per attribuut een verschil is tussen de overlap van goedaardig en kwaadaardig. Hieruit is eventueel te halen welke attributen het beste te gebruiken zijn voor machine learning, namelijk de attributen met de minste overlap. De attributen uniformity of cell size en uniformity of cell shape zijn ook hierbij goed te gebruiken voor een machine learning programma, doordat ze weinig overlap met elkaar hebben. De ID nummers van de kwaadaardige tumoren zijn hierin relatief hoog, terwijl de ID nummers van de goedaardige tumoren relatief laag zijn. Hierin is ook nog duidelijker te zien dat het attribuut mitoses minder handig is om te gebruiken.
Attribuut BareNulei heeft ook relatief weinig overlap, dus deze is ook mogelijk goed bruikbaar.



```{r warning = FALSE}
cbbPalette <- c("#66bb6a", "#ef5350")

jitter_plot <- ggplot(data.melt) + 
  geom_jitter(aes(x = ClassName ,y = value, colour=ClassName), alpha = 0.5)+
  facet_wrap(~variable, ncol=3, scales='free')+
  ggtitle("Correlation between benign/malignant tumor and attribute") +
  xlab("Benign or malignant tumor") +
  ylab("ID numbers") + 
  scale_colour_manual(values=cbbPalette)

jitter_plot
```

#Week 3
In week 3 zijn de laatste figuren afgemaakt, zoals de heatmap en de scatterplot om de korrelatie te bekijken tussen de attributen.
Over het algemeen lijkt dit me een goede dataset, waarbij eventueel de rijen met missende waarden eruitgehaald kunnen worden. Deze missende waarden staan echter wel in een attribuut die iets minder te gebruiken is voor het maken van een machine learning programma. Er is meer overlap in de ID nummers van goedaardige en kwaadaardige tumoren te zien dan bij sommige andere attributen, zoals "uniformity of cell size" en "uniformity of cell shape".
De missende waarden zijn alleen wel vervelend voor sommige algoritmen, dus ze gaan er wel uit. 
Het bestand voor WEKA heeft attribuutnamen, geen missende waarden en de originele kolom Class is vervangen door een kolom ClassName waarin nominale waarden staan: benign en malignant. In het originele bestand werden deze groepen aangegeven met 2 voor benign en 4 voor malignant.

Hieronder is een heatmap te zien van de covarianties van de dataset. Hierin is af te lezen dat er relatief veel korrelatie zit tussen uniformity of cell size en uniformity of cell shape. Dit was in de boxplot en jitter plot ook al redelijk goed te zien, maar dit figuur bevestigt dit. Voor de rest is de korrelatie niet heel hoog tussen de andere attributen. Het valt nog wel op dat tussen mitoses en andere attributen weinig korrelatie zit. Deze vakjes zijn allemaal in het blauw aangegeven. 
Het is interessant om de korrelatie tussen uniformity of cell shape en uniformity of cell size beter te bekijken in een plot.

```{r}
complete.Dataset1 <- dataset1[complete.cases(dataset1), ]

covariance1 <- cor(x = complete.Dataset1[2:10])
pheatmap(covariance1)
```


Hieronder is de plot die de korrelatie tussen uniformity of cell shape en uniformity of cell size beter laat zien. De punten lijken ver uit elkaar te zitten in het figuur, dit komt omdat er in de dataset ID nummers staan. Dit zijn integers, dus hele getallen zonder komma. De getallen lopen van 1 tot 10. Hierdoor zit er relatief veel ruimte tussen de punten in de plot. Er is wel een diagonale lijn doorheen te trekken waarbij het zo is dat hoe hoger de ID nummers, hoe groter de kans dat de tumor kwaadaardig is en hoe lager de ID nummers zijn, hoe groter de kans dat de tumor goedaardig is.


```{r}
cbbPalette <- c("#66bb6a", "#ef5350")

jitter.Plot <- ggplot() + 
  geom_jitter(aes(x = dataset1$UniformityOfCellSize, y = dataset1$UniformityOfCellShape, colour = dataset1$ClassName), alpha = 0.3) +
  ggtitle("Correlation between uniformity of cell size and -shape.") +
  xlab("Uniformity of cell size") +
  ylab("Uniformity of cell shape") +
  labs(colour = "Legend") +
  theme(legend.title = element_text(colour = "black", size = 10, face = "bold")) +
  scale_colour_manual(values=cbbPalette)

jitter.Plot
```

Nieuw bestand maken voor weka. Deze bevat een header, dit is de dataset waarmee gewerkt is in dit script. De missing values zijn er ook uitgehaald en de kolom class. Class zat in de originele dataset en hier stonden numerieke waarden in. In de nieuwe kolom die erin is gebleven voor weka staan nominale waarden.
```{r}
write.csv(complete.Dataset1[c(1:10, 12)], "../data/breast_cancer.csv")
```

Resultaat Weka experimenter zeroR, oneR & RandomForest

```{r}
results <- read.csv("../data/breast_cancer_results.csv", 
                    header = TRUE, 
                    sep = ",", 
                    na.strings = "?")
```


#Week 4
In week 4 ben ik verder gegaan met het nieuwe bestand zonder missende waarden, met kolomnamen en een vervangen attribuut. Dit bestand heb ik ingeladen in weka en hier heb ik een aantal algoritmen/classifiers op uitgevoerd.

Bij elke classifier is 10-fold cross validation gebruikt.
De precieze uitkomsten en instellingen per algoritme staan in het verslag.

#Beste algoritme
SMO heeft het hoogste percentage goed, maar randomForest heeft een lager FN rate en heeft 6 staan in plaats van 8 bij SMO in de categorie classified as benign terwijl de tumor kwaadaardig is. De percentages schelen zo minimaal dat dit verwaarloosbaar is. De ROC area is bij RandomForest 0,994 en bij SMO 0,971. Dit is ook een verschil, waardoor RandomForest een betere keus zal zijn.
Door een iets betere False Negative rate kies ik RandomForest als algoritme.

#Week 5
De algoritmes zijn uitgevoerd in Weka experimenter.
Vervolgens is hier een ROC curve van gemaakt in weka explorer, zie bestand ROC_curve.bmp in het mapje figures.
Als laatste staat er een learning curve van het algoritme SMO in het bestand project.Rmd waarin te zien is dat het verstandig is om 90% van de data te houden, want de lijn stijgt behoorlijk zodra je er een percentage extra aan data uithaalt.
Resultaten en conclusie & discussie geschreven en begonnen aan Java wrapper program.

De ideale instellingen zijn voor RandomForest: weka.classifiers.trees.RandomForest -P 100 -I 100 -num-slots 1 -K 1 -M 1.0 -V 0.001 -S 1 -depth 5
Het model met deze instellingen is opgeslagen en staat in het mapje data van de Java wrapper.

Learning curve 

```{r}
data.Taken.Out <- c(10, 20, 30, 40, 50, 60, 70, 80, 90)
error.Rate <- c(2.54, 2.89, 3.02, 3.32, 3.54, 3.67, 3.88, 4.05, 4.40)
learning.Curve <- data.frame(data.Taken.Out, error.Rate)

ggplot(learning.Curve) + 
  geom_smooth(aes(x = data.Taken.Out, y = error.Rate)) +
  ggtitle("Learning curve RandomForest") +
  xlab("Percentage data taken out of the dataset") +
  ylab("Error rate")
```

Hierboven is de learning curve te zien. Het lijkt me verstandig om 90% van de data te houden, omdat het met deze dataset accuracy het belangrijkst is. De lijn gaat bijna lineair omhoog, dus elke procent die eruit gaat heeft effect op de accuracy. 
Deze learning curve is gemaakt met het algoritme RandomForest.


```{r}
algorithm <- c("ZeroR",
               "oneR",
                "Naivebayes",
                "J48 tree", 
                "Nearest neighbor",
                "SMO",
                "SimpleLogistic",
                "RandomForest")
percentage.Correct <- c(65.0, 91.8, 96.3, 96.0, 96.5, 97.2, 96.5, 97.1)
false.Positive.Rate <- c(0.650, 0.108, 0.031, 0.043, 0.046, 0.030, 0.046, 0.027)
outcomes <- data.frame(algorithm, percentage.Correct, false.Positive.Rate)

colnames(outcomes) <- c("algorithm", "Percentage correct", "False positive rate")

pander(outcomes, caption = "Percentage correct & FPR from different algorithms WEKA")
```

```{r}
classified.As <- c("Benign", "Malignant")
benign <- c(430, 6)
malignant <- c(14, 233)

confusion.Matrix <- data.frame(classified.As, benign, malignant)

colnames(confusion.Matrix) <- c("RandomForest", "Benign", "Malignant")

pander(confusion.Matrix, caption = "Confusion Matrix algorithm RandomForest WEKA")
```



```{r}
classified.As <- c("Benign", "Malignant")
benign <- c(433, 8)
malignant <- c(11, 231)

confusion.Matrix <- data.frame(classified.As, benign, malignant)

colnames(confusion.Matrix) <- c("SMO", "Benign", "Malignant")

pander(confusion.Matrix, caption = "Confusion matrix algorithm SMO WEKA")
```

   a   b   <-- classified as
 433  11 |   a = Benign
   8 231 |   b = Malignant
   
   
#Week 6 & 7:
Verder gegaan met de java wrapper, zie de bestanden WekaRunner.java, ArgParserCommandline.java en ArgRunner.java. Deze staan in een aparte bitbucket repository: thema09.breast.cancer. Deze java programma's zorgen ervoor dat het model uit te voeren is vanaf de commandline met een inputbestand (de training- en testdata), een unknown bestand (hiervan moeten de klassen ingevuld worden aan de hand van de attributen) en een output bestand waar het unknownbestand inzit met de ingevulde klassenwaarden.
Daarnaast is een begin gemaakt met het verslag en zijn er README bestanden toegevoegd op de repositories aan de hand van deze template: 
https://gist.github.com/PurpleBooth/109311bb0361f32d87a2
   


