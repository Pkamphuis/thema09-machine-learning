# Java Wrapper predicting breast cancer with machine learning

Java wrapper to predict benign and malignant breast cancer tumors based on nine attributes with the RandomForest algorithm.


## Getting started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

What things you need to install the software and how to install them

[Java 1.8.0](https://www.java.com/en/download/)


### Installing

For the installation of this package you will need to clone this project onto your machine. This can be done with the following command.

```
git clone https://Pkamphuis@bitbucket.org/Pkamphuis/thema09.breast.cancer.git
```

### Usage

Go to this parh in this project: \out\artifacts\thema09_breast_cancer_jar
and run it like this in the commandline:

```
 java -jar thema09.breast.cancer.jar -i data\breast_cancer.arff -u data\unknown_breast_cancer.arff -o out.arff
```
-u means the file with instances without the right class and -o is the outputfile, name the file as you want. 

## Running the tests

Explain how to run the automated tests for this system


##Deployment

Add additional notes about how to deploy this on a live system


## Built With

* [Weka](https://www.cs.waikato.ac.nz/ml/weka/)
* [CLIdemo](https://bitbucket.org/minoba/clidemo/)


## Authors

* **Priscilla Kamphuis** - [Pkamphuis](https://bitbucket.org/Pkamphuis/)

## License

/*
* Copyright (c) 2018 <Priscilla Kamphuis>.
* Licensed under GPLv3. See gpl.md
*/



