/*
 * Copyright (c) 2018 <Priscilla Kamphuis>.
 * Licensed under GPLv3. See gpl.md
 */
/*
 * Copyright (c) 2018 <Priscilla Kamphuis>.
 * Licensed under GPLv3. See gpl.md
 */

//package name
package nl.bioinf.pkamphuis.breast_cancer;

//imports
import java.util.Arrays;

/**
* This class runs the program on commandline with the right arguments and catches errors if needed.
* This class prints errors in a user-friendly way.
*/


public final class ArgRunner {
    private ArgRunner() {
    }

    /**
     * This function checks if the right arguments are given.
     * @param args Commandline arguments are given.
     */
    public static void main(final String[] args) {
        try {
            ArgParserCommandline op = new ArgParserCommandline(args);
            if (op.helpRequested()) {
                op.printHelp();
                return;
            }if(!op.checkPassed(new String[]{"i","u","o"})){
                System.err.println("You need to supply the correct format. Correct format listed in help!");
                op.printHelp();
                System.exit(1);
            }

            // Run program with other classes WekaRunner and ArgParserCommandline
            WekaRunner wr = new WekaRunner();
            wr.start(op.getArgument("i"),op.getArgument("u"),op.getArgument("o"));

            // Print user-friendly error if something went wrong.
        } catch (IllegalStateException ex) {
            System.err.println("Something went wrong while processing your command line \""
                    + Arrays.toString(args) + "\"");
            System.err.println("Parsing failed.  Reason: " + ex.getMessage());
            ArgParserCommandline op = new ArgParserCommandline(new String[]{});
            op.printHelp();
    }
    }
}

