/*
 * Copyright (c) 2018 <Priscilla Kamphuis>.
 * Licensed under GPLv3. See gpl.md
 */
/*
 * Copyright (c) 2018 <Priscilla Kamphuis>.
 * Licensed under GPLv3. See gpl.md
 */



package nl.bioinf.pkamphuis.breast_cancer;

//Inputfile mag CSV of arff zijn
//Eventueel kiezen wat voor output eruit komt bij outputfile. Alle data er nog bij in of alleen een bestand met de
//klassenlabels in goede volgorde.

import org.apache.commons.cli.*;

/**
 *This class sets the right commandline arguments.
 *
 */

public class ArgParserCommandline {

    /**
     * Declare different arguments help, inputfile, unknownfile and outputfile
     */
    private static final String HELP = "HELP";
    private static final String INPUTFILE = "file.arff";
    private static final String UNKNOWNFILE = "unkownfile.arff";
    private static final String OUTPUTFILE = "outputfile.arff";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * Initialize arguments from previous function.
     * @param args commandline arguments are given, declared in previous function.
     */
    public ArgParserCommandline(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Make sure the arguments are parsed and the options/arguments are defined
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * This function parses the commandline and print error if wrong arguments are given.
     */
    private void processCommandLine() {
        CommandLineParser parser = new DefaultParser();
        try {
            this.commandLine = parser.parse(this.options, this.clArguments);
        }catch(ParseException e){
            System.err.println(e);
        }
    }

    /**
     * Help getter
     * @return if the user wants help
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }

    /**
     * Specific argument getter
     * @param name name variables
     * @return String argument linked to argument Name
     */
    public String getArgument(String name){
        return this.commandLine.getOptionValue(name);
    }

    /**
     * Checks if specified arguments are given
     * @param args arguments in string array ["i", "u", "o"]
     * @return true (arguments given) or false (no every arguments given)
     */
    public boolean checkPassed(String[] args){
        for (String arg:args
             ) {
            if(!this.commandLine.hasOption(arg)){
                return false;
            }
        }
        return true;
    }

    /**
     * This function builds the commandline options.
     */
    private void buildOptions() {
        // create Options object
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Input: Programname.java -i inputfile.arff -u unknownfile.arff");
        Option inputOption = new Option("i", INPUTFILE, true, "Inputfile in .arff format");
        Option unknownfileOption = new Option("u", UNKNOWNFILE, true, "Unknownfile in .arff format");
        Option outputOption = new Option("o", OUTPUTFILE, true, "outputfile in .arff format");
        options.addOption(helpOption);
        options.addOption(inputOption);
        options.addOption(unknownfileOption);
        options.addOption(outputOption);
    }

    /**
     * Prints user-friendly help
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("JavaWrapper", options);
    }
}


