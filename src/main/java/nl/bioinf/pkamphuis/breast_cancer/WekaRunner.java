/*
* Copyright (c) 2018 <Priscilla Kamphuis>.
* Licensed under GPLv3. See gpl.md
*/
/*
 * Copyright (c) 2018 <Priscilla Kamphuis>.
 * Licensed under GPLv3. See gpl.md
 */

package nl.bioinf.pkamphuis.breast_cancer;

// imports for using weka model
import weka.classifiers.trees.RandomForest;
        import weka.core.Instance;
        import weka.core.Instances;
        import weka.core.converters.ConverterUtils.DataSource;

import java.io.*;

/**
 * This class defines the RandomForest model and executes it.
 * The WekaRunner can run without ArgParserCommandline and ArgRunner
 * and uses the original test files without giving the program arguments.
 */
public class WekaRunner {
    private final String modelFile = "data/RandomForest.model";
    //Declare WekaRunner when this file is executed
    public static void main(String[] args) {
        WekaRunner runner = new WekaRunner();
        runner.startDefault();
    }

    /**
     * Declare default files inputfile, unknownfile and outputfile.
     */
    private void startDefault(){
        String dataFile = "data/breast_cancer.arff";
        String unknownFile = "data/unknown_breast_cancer.arff";
        String outputFile = "data/outputile.arff";
        this.start(dataFile,unknownFile,outputFile);
    }

    /**
     * Loads and processes the specified files, then writes result into outputFile
     * @param dataFile training file to train the model RandomForest
     * @param unknownFile unknown file with instances without classes
     * @param outputFile unknownfile with filled in the right classes
     */
    public void start(String dataFile, String unknownFile, String outputFile) {

        try {
            Instances instances = loadArff(dataFile);
            printInstances(instances);
            RandomForest fromFile = loadClassifier();
            Instances unknownInstances = loadArff(unknownFile);
            System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            Instances i = classifyNewInstance(fromFile, unknownInstances);
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(outputFile), "utf-8"))) {
                writer.write(i.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This function classifies each instance from the unknownfile with the right class
     * @param tree RandomForest model tree
     * @param unknownInstances unknown instances from the unknownfile without the right class
     * @return labeled instances with the right class
     * @throws Exception
     */
    private Instances classifyNewInstance(RandomForest tree, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = tree.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        //optional line to print output in terminal
        //System.out.println("\nNew, labeled = \n" + labeled);
        return labeled;
    }

    /**
     * This function loads the RandomForest model
     * @return RandomForest model
     * @throws Exception
     */
    private RandomForest loadClassifier() throws Exception {
        // deserialize model
        return (RandomForest) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * print instances with the right class
     * @param instances
     */

    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();
        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());

        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

    /**
     * Load specified dataFile
     * @param dataFile path to dataset in .arff format
     * @return Instances from dataFile
     * @throws IOException
     */
    private Instances loadArff(String dataFile) throws IOException {
        try {
            DataSource source = new DataSource(dataFile);
            Instances data = source.getDataSet();
            // setting class attribute if the data format does not provide this information
            // For example, the .arff format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);

            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }
}

